import {
  callFBSendAPI,
  callFBUserAPI,
} from '../helpers/fbApi';
import {
  callNewUserAPI,
} from '../helpers/api';
import {
  sendSenderAction,
} from '../helpers/cta';
import quickReplies from './../data/quickReplies';

export const handleGetStarted = (senderId) => {
  sendSenderAction(senderId);

  callFBUserAPI(senderId, (err, user) => {
    if (err) {
      console.error(`Unable to get user profile: ${err}`);
    } else {
      callNewUserAPI(user);
    }
  });

  const welcome = {
    "text": "Welcome to Palsaik MY! Click on the Menu button below to see what you can do with us."
  };

  callFBSendAPI(senderId, welcome);

  const response = {
    "text"         : "You can also click on any of the options below!",
    "quick_replies": quickReplies
  }

  sendSenderAction(senderId);

  setTimeout(() => {
    callFBSendAPI(senderId, response);
  }, 5000);
}