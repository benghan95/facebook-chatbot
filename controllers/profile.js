import {
  callFBProfileAPI
} from '../helpers/fbApi';
import settings from '../data/profileSettings';

export const resetProfile = (req, res) => {
  const properties = {
    "fields": [
      "greeting",
      "get_started",
      "persistent_menu",
    ]
  };

  callFBProfileAPI('DELETE', properties);

  res.status(200).send({
    'status': 'success',
    'results': 'OK'
  });
};

export const setProfile = (req, res) => {
  callFBProfileAPI('POST', settings);
  res.status(200).send({
    'status': 'success',
    'results': 'OK'
  });
};