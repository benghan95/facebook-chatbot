import {
  callFBSendAPI,
} from '../helpers/fbApi';
import {
  sendReserveCTA,
  sendSenderAction,
} from '../helpers/cta';
import quickReplies from './../data/quickReplies';
import menuOptions from '../data/menuOptions';
import setMealsMenu from '../data/menu/setMeals';
import alaCarteMenu from '../data/menu/alaCarte';
import extrasMenu from '../data/menu/extras';
import alcoholMenu from '../data/menu/alcohol';
import nonAlcoholMenu from '../data/menu/nonAlcohol';

const sendMenu = (senderId, menu) => {
  sendSenderAction(senderId);

  menu.forEach(element => {
    const message = {
      "attachment": {
        "type": "image",
        "payload": {
          "url"        : element.url,
          "is_reusable": element.is_reusable
        }
      }
    };

    callFBSendAPI(senderId, message);
  });
}

export const handleViewMenu = (senderId) => {
  const response = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type"    : "list",
        "top_element_style": "compact",
        "elements"         : menuOptions,
      }
    },
    "quick_replies": quickReplies
  };

  callFBSendAPI(senderId, response);
}

export const handleViewSetMeals = (senderId) => {
  sendMenu(senderId, setMealsMenu);
  sendReserveCTA(senderId)
}

export const handleViewAlaCarte = (senderId) => {
  sendMenu(senderId, alaCarteMenu);
  sendReserveCTA(senderId)
}

export const handleViewExtras = (senderId) => {
  sendMenu(senderId, extrasMenu);
  sendReserveCTA(senderId)
}

export const handleViewBeverage = (senderId) => {
  sendSenderAction(senderId);

  const response = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "button",
        "text"         : "Which type of beverage menu do you want to view?",
        "buttons"     : [
          {
            "title"  : "Alcohol Menu",
            "type"   : "postback",
            "payload": "VIEW_ALCOHOL"
          },
          {
            "title"  : "Non-alcohol Menu",
            "type"   : "postback",
            "payload": "VIEW_NON_ALCOHOL"
          }
        ],
      }
    },
    "quick_replies": quickReplies
  };

  callFBSendAPI(senderId, response);
}

export const handleViewAlcohol = (senderId) => {
  sendMenu(senderId, alcoholMenu);
  sendReserveCTA(senderId);
}

export const handleViewNonAlcohol = (senderId) => {
  sendMenu(senderId, nonAlcoholMenu);
  sendReserveCTA(senderId);
}