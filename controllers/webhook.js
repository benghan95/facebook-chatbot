
import {
    handleMessage,
    handlePostback,
} from './../helpers/processMessage';

export const verification = (req, res) => {

    // Your verify token. Should be a random string.
    let VERIFY_TOKEN = "graphicmaster";

    // Parse the query params
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    // Checks if a token and mode is in the query string of the request
    if (mode && token) {

        // Checks the mode and token sent is correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Responds with the challenge token from the request
            res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            res.sendStatus(403);
        }
    }

}

export const messageWebhook = (req, res) => {

    let body = req.body;

    // Checks this is an event from a page subscription
    if (body.object === 'page') {

        // Iterates over each entry - there may be multiple if batched
        body.entry.forEach((entry) => {
            entry.messaging.forEach((event) => {
                if (event.message) {
                    console.log("Message received");
                    handleMessage(event.sender.id, event.message);
                } else if (event.postback) {
                    console.log("Postback received");
                    handlePostback(event.sender.id, event.postback);
                } else {
                }
            })

        });

        // Returns a '200 OK' response to all requests
        res.status(200).end();
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }

}