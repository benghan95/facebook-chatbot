import {
  callFBSendAPI,
} from '../helpers/fbApi';
import {
  sendSenderAction
} from '../helpers/cta';
import outlets from './../data/outlets.js';
import quickReplies from './../data/quickReplies';

export const handleOutletsAvailable = (senderId) => {
  sendSenderAction(senderId);

  const response = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "generic",
        "elements"     : outlets
      }
    },
    "quick_replies": quickReplies
  }

  callFBSendAPI(senderId, response);
}

export const handleNearestOutlet = (senderId) => {
  sendSenderAction(senderId);

  const response = {
    "text": "Share us your location so that we can find you the nearest Palsaik outlet.",
    "quick_replies": [{
      "title"       : "Share my location",
      "content_type": "location",
    }]
  }

  callFBSendAPI(senderId, response);
}

export const findNearestOutlet = (senderId, coordinates) => {
  sendSenderAction(senderId);

  const lat  = coordinates.lat;
  const long = coordinates.long;
}

export const handleViewOutletSolaris = (senderId) => {

}
export const handleViewOutletScott = (senderId) => {

}
export const handleViewOutletJohor = (senderId) => {

}
export const handleViewOutletMalacca = (senderId) => {

}
export const handleViewOutletPJ = (senderId) => {

}
export const handleViewOutletMyTown = (senderId) => {

}
export const handleViewOutletGenting = (senderId) => {

}