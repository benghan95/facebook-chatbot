import {
  callFBSendAPI,
} from '../helpers/fbApi';
import {
  sendSenderAction
} from '../helpers/cta';
import quickReplies from './../data/quickReplies';

export const handleFAQ = (senderId) => {
  sendSenderAction(senderId);

  const response = {
    "attachment":{
      "type"   : "template",
      "payload": {
        "template_type": "button",
        "text"         : "For operating hours, please go to \"Outlets Available\". As we have several FAQs, we suggest you to view the FAQ from our website.",
        "buttons"      : [
          {
            "title"  : "Outlets Available",
            "type"   : "postback",
            "payload": "OUTLETS_AVAILABLE",
          },
          {
            "title": "View FAQs",
            "type" : "web_url",
            "url"  : "https://palsaik.axblab.com/",
          },
        ]
      }
    },
    "quick_replies": quickReplies
  };

  callFBSendAPI(senderId, response);
}