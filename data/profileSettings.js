export default {
  "greeting": [
    {
      "locale": "default",
      "text"  : "Hello {{user_first_name}}! Click on \"Get Started\" to chat with us! You can make reservation, view menu and even find the nearest outlets here."
    }
  ],
  "get_started": {
    "payload": "GET_STARTED"
  },
  "persistent_menu": [
    {
      "locale"                 : "default",
      "composer_input_disabled": false,
      "call_to_actions"        : [
        {
          "title"  : "Manage reservation",
          "type"   : "nested",
          "call_to_actions": [
            {
              "title"  : "Make a reservation",
              "type"   : "postback",
              "payload": "MAKE_RESERVATION",
            },
            {
              "title"  : "Cancel my reservation",
              "type"   : "postback",
              "payload": "CANCEL_RESERVATION",
            },
          ]
        },
        {
          "title"  : "Find the nearest Palsaik",
          "type"   : "postback",
          "payload": "NEAREST_OUTLET",
        },
        {
          "title"  : "Other",
          "type"   : "nested",
          "call_to_actions": [
            {
              "title"  : "View food menu",
              "type"   : "postback",
              "payload": "VIEW_MENU",
            },
            {
              "title"  : "Palsaik MY Outlets",
              "type"   : "postback",
              "payload": "OUTLETS_AVAILABLE",
            },
            {
              "title"  : "FAQ",
              "type"   : "postback",
              "payload": "FAQ",
            },
          ]
        },
      ]
    }
  ],
}