export default [
  {
    "title"    : "Set Meals",
    "subtitle" : "Our seat meals consist of ...",
    "buttons"  : [{
      "title"  : "View",
      "type"   : "postback",
      "payload": "VIEW_SET_MEALS"
    }],
  },
  {
    "title"    : "À la carte",
    "subtitle" : "À la carte ......",
    "buttons"  : [{
      "title"  : "View",
      "type"   : "postback",
      "payload": "VIEW_ALA_CARTE"
    }],
  },
  {
    "title"    : "Extras",
    "subtitle" : "Extras ......",
    "buttons"  : [{
      "title"  : "View",
      "type"   : "postback",
      "payload": "VIEW_EXTRAS"
    }],
  },
  {
    "title"    : "Beverage",
    "subtitle" : "Beverage ......",
    "buttons"  : [{
      "title"  : "View",
      "type"   : "postback",
      "payload": "VIEW_BEVERAGE"
    }],
  },
];