export default [
  {
    "title"       : "Make a reservation",
    "content_type": "text",
    "payload"     : "MAKE_RESERVATION",
  },
  {
    "title"       : "View food menu",
    "content_type": "text",
    "payload"     : "VIEW_MENU",
  },
  {
    "title"       : "Find nearest outlet",
    "content_type": "text",
    "payload"     : "NEAREST_OUTLET",
  },
  {
    "title"       : "Outlets available",
    "content_type": "text",
    "payload"     : "OUTLETS_AVAILABLE",
  },
  {
    "title"       : "FAQ",
    "content_type": "text",
    "payload"     : "FAQ",
  },
  {
    "title"       : "Cancel my reservation",
    "content_type": "text",
    "payload"     : "CANCEL_RESERVATION",
  },
]