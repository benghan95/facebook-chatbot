export default [
  {
    "title": "Solaris Mont Kiara",
    "subtitle": "J-01-09, SohoKL, Solaris Mont Kiara, No. 2 Jalan Solaris, 50480 KL. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_SOLARIS"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_SOLARIS"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+60362110383"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_SOLARIS"
      }
    ]
  },
  {
    "title": "Scott Garden",
    "subtitle": "2-15 & 16, The Scott Garden, 289 Jalan Kelang Lama, 58000 KL. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_SCOTT"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_SCOTT"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+60379727283"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_SCOTT"
      }
    ]
  },
  {
    "title": "Johor Bahru",
    "subtitle": "No. 51, Jalan Sutera Tanjung 8/2 Taman Sutera Utama 81300 Skudai, Johor. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_JOHOR"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_JOHOR"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+6075563833"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_JOHOR"
      }
    ]
  },
  {
    "title": "Malacca",
    "subtitle": "No 58 & 60, Jalan Melaka Raya 23,Taman Melaka Raya, 75000, Melaka. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_MALACCA"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_MALACCA"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+6062818623"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_MALACCA"
      }
    ]
  },
  {
    "title": "Kota Damansara",
    "subtitle": "B-08-G, Block B, No.2 Jalan PJU 5/14, PJU 5, Kota Damansara 47810, Petaling Jaya, Selangor. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_PJ"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_PJ"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+60361513338"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_PJ"
      }
    ]
  },
  {
    "title": "MyTown",
    "subtitle": "Lot No. L3-022 & L3-E-022, Level 3, myTOWN, No.6 , Jalan Cochrane, Seksyen 90, 55100 KL. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_MYTOWN"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_MYTOWN"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+60327152080"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_MYTOWN"
      }
    ]
  },
  {
    "title": "Genting",
    "subtitle": "Lot No.T2C-08, Level 4, SkyAvenue, Genting Highlands Resort, Pahang. \r\n Operating hours: 10:00AM - 10:00PM",
    // "default_action": {
    //   "title"  : "View outlet",
    //   "type"   : "postback",
    //   "payload": "VIEW_OUTLET_GENTING"
    // },
    "buttons": [
      {
        "title"  : "Make reservation",
        "type"   : "postback",
        "payload": "RESERVE_GENTING"
      },
      {
        "title"  : "Call us now",
        "type"   : "phone_number",
        "payload": "+60361013933"
      },
      {
        "title"  : "View outlet",
        "type"   : "postback",
        "payload": "VIEW_OUTLET_GENTING"
      }
    ]
  }
]