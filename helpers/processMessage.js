import apiAi from 'apiai';

import {
  callFBSendAPI,
  callFBUserAPI
} from './fbApi';
import {
  callNewUserAPI,
} from './api';
import {
  handleGetStarted
} from '../controllers/getStarted';
import {
  handleMakeReservation,
  handleReserveSolaris,
  handleReserveScott,
  handleReserveJohor,
  handleReserveMalacca,
  handleReservePJ,
  handleReserveMyTown,
  handleReserveGenting,
  handleCancelReservation,
} from '../controllers/reservation';
import {
  handleViewMenu,
  handleViewSetMeals,
  handleViewAlaCarte,
  handleViewExtras,
  handleViewBeverage,
  handleViewAlcohol,
  handleViewNonAlcohol,
} from '../controllers/menu';
import {
  findNearestOutlet,
  handleNearestOutlet,
  handleOutletsAvailable,
  handleViewOutletSolaris,
  handleViewOutletScott,
  handleViewOutletJohor,
  handleViewOutletMalacca,
  handleViewOutletPJ,
  handleViewOutletMyTown,
  handleViewOutletGenting,
} from '../controllers/outlets';
import {
  handleFAQ
} from '../controllers/faq';

const API_AI_TOKEN = '4957427d87d9442b946787bf8fdc2c87';
const apiAiClient  = apiAi(API_AI_TOKEN);

// Handles messaging_postbacks events
export const handlePostback = (senderId, receivedPostback) => {
  const payload = receivedPostback.payload;
  console.log(payload);

  let response;

  switch (payload) {
    case 'GET_STARTED':
      handleGetStarted(senderId);
      break;

    case 'MAKE_RESERVATION':
      handleMakeReservation(senderId);
      break;

    case 'RESERVE_SOLARIS':
      handleReserveSolaris(senderId);
      break;

    case 'RESERVE_SCOTT':
      handleReserveScott(senderId);
      break;

    case 'RESERVE_JOHOR':
      handleReserveJohor(senderId);
      break;

    case 'RESERVE_MALACCA':
      handleReserveMalacca(senderId);
      break;

    case 'RESERVE_PJ':
      handleReservePJ(senderId);
      break;

    case 'RESERVE_MYTOWN':
      handleReserveMyTown(senderId);
      break;

    case 'RESERVE_GENTING':
      handleReserveGenting(senderId);
      break;

    case 'CANCEL_RESERVATION':
      handleCancelReservation(senderId);
      break;

    case 'NEAREST_OUTLET':
      handleNearestOutlet(senderId);
      break;

    case 'VIEW_MENU':
      handleViewMenu(senderId);
      break;

    case 'VIEW_SET_MEALS':
      handleViewSetMeals(senderId);
      break;

    case 'VIEW_ALA_CARTE':
      handleViewAlaCarte(senderId);
      break;

    case 'VIEW_EXTRAS':
      handleViewExtras(senderId);
      break;

    case 'VIEW_BEVERAGE':
      handleViewBeverage(senderId);
      break;

    case 'VIEW_ALCOHOL':
      handleViewAlcohol(senderId);
      break;

    case 'VIEW_NON_ALCOHOL':
      handleViewNonAlcohol(senderId);
      break;

    case 'OUTLETS_AVAILABLE':
      handleOutletsAvailable(senderId);
      break;

    case 'VIEW_OUTLET_SOLARIS':
      handleViewOutletSolaris(senderId);
      break;

    case 'VIEW_OUTLET_SCOTT':
      handleViewOutletScott(senderId);
      break;

    case 'VIEW_OUTLET_JOHOR':
      handleViewOutletJohor(senderId);
      break;

    case 'VIEW_OUTLET_MALACCA':
      handleViewOutletMalacca(senderId);
      break;

    case 'VIEW_OUTLET_PJ':
      handleViewOutletPJ(senderId);
      break;

    case 'VIEW_OUTLET_MYTOWN':
      handleViewOutletMyTown(senderId);
      break;

    case 'VIEW_OUTLET_GENTING':
      handleViewOutletGenting(senderId);
      break;

    case 'FAQ':
      handleFAQ(senderId);
      break;
  }
}

export const handleMessage = (senderId, receivedMessage) => {
  if (receivedMessage.text) {
    const message = receivedMessage.text;

    switch (message){
      case 'Get Started':
        handleGetStarted(senderId);
        break;

      case 'Make a reservation':
        handleMakeReservation(senderId);
        break;

      case 'Cancel my reservation':
        handleCancelReservation(senderId);
        break;

      case 'Find the nearest Palsaik':
      case 'Find nearest Palsaik':
      case 'Find nearest outlet':
        handleNearestOutlet(senderId);
        break;

      case 'View food menu':
        handleViewMenu(senderId);
        break;

      case 'Outlets available':
        handleOutletsAvailable(senderId);
        break;

      case 'FAQ':
        handleFAQ(senderId);
        break;

      default:
        const apiaiSession = apiAiClient.textRequest(message, {
          sessionId: 'palsaik_chatbot'
        });

        apiaiSession.on('response', (response) => {
          const result = response.result.fulfillment.speech;
          const text = {
            "text": result
          };

          callFBSendAPI(senderId, text);
        });

        apiaiSession.on('error', error => console.error(error));
        apiaiSession.end();
        break;
    }

  } else if (receivedMessage.attachments) {
    const attachments = receivedMessage.attachments;

    // Gets the URL of the message attachment
    let attachment_url = attachments[0].payload.url;

    if (attachments[0].type === 'location') {
      const coordinates = attachments[0].payload.coordinates;

      findNearestOutlet(senderId, coordinates);
    }
  }

};