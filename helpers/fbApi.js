import { parse } from 'path';
import axios from 'axios';

const request = require('request');

const FACEBOOK_ACCESS_TOKEN = 'EAADmuR63ZC9YBAE00dsVF5ZC1nk8ObODLySkVuvwSZAfls2oTfIBMkLkU1rRCZCVbl8BlYG1ZA21Legbq4Mx8K6Mhv7KlWoRyah1enI6XLv0cO7jpzbb6zipr3YF166dZBIZAUNjbBWM79ZBPo3CDp0U4q95aj0Hy4VuBsGWYh1wgwZDZD';

export const callFBSendAPI = (senderId, response, attr) => {
  let json = {
    "recipient": { "id": senderId },
    "message"  : response,
  };

  if (attr) {
    json = attr;
  }

  request({
    url   : 'https://graph.facebook.com/v2.6/me/messages',
    method: 'POST',
    qs    : { access_token: FACEBOOK_ACCESS_TOKEN },
    json
  }, (err, res, body) => {
    if (err) {
      console.error(`Unable to send message via SendAPI: ${err}`);
    } else {
      if (body.error) {
        console.error(`Error on sending message: ${body.error.message}`);
      } else {
        console.log("Messenger sent through SendAPI!");
      }
    }
  });
}

export const callFBProfileAPI = (method, json) => {
  request({
    url: 'https://graph.facebook.com/v2.6/me/messenger_profile',
    qs : { access_token: FACEBOOK_ACCESS_TOKEN },
    method,
    json
  }, (err, res, body) => {
    if (err) {
      console.error(`Unable to send ${method} message: ${err}`);
    } else {
      if (body.error) {
        console.error(`Error on updating messenger profile: ${body.error.message}`);
      } else {
        console.log("Messenger profile has been updated successfully");
      }
    }
  });
}

export const callFBUserAPI = (psId, callback) => {
  axios.get(`https://graph.facebook.com/v2.6/${psId}`, {
    params: {
      access_token: FACEBOOK_ACCESS_TOKEN,
      fields      : "first_name,last_name,profile_pic,gender,locale"
    }
  })
  .then((res) => {
    console.log(res);

    // const user = JSON.parse(body);
    // if (err) {
    //   callback(err, null);
    // } else {
    //   if (user.first_name) {
    //     callback(null, user);
    //   } else {
    //     callback('Error on requesting user profile from User API', null);
    //   }
    // }
  })
  // request({
  //   url   : `https://graph.facebook.com/v2.6/${psId}`,
  //   method: 'GET',
  //   qs    : {
  //     access_token: FACEBOOK_ACCESS_TOKEN,
  //     fields      : "first_name,last_name,profile_pic,gender,locale"
  //   },
  // }, (err, res, body) => {
    
  // });
}
