import {
  callFBSendAPI,
} from '../helpers/fbApi';
import quickReplies from './../data/quickReplies';

export const sendSenderAction = (senderId) => {
  const attr = {
    "recipient": {
      "id": senderId
    },
    "sender_action": "typing_on"
  };

  callFBSendAPI(senderId, null, attr);
}

export const sendReserveCTA = (senderId) => {
  const response = {
    "attachment": {
      "type"   : "template",
      "payload": {
        "template_type": "button",
        "text"         : "Make a reservation now or choose any replies below",
        "buttons"      : [
          {
            "title"  : "Make a reservation",
            "type"   : "postback",
            "payload": "MAKE_RESERVATION",
          },
        ]
      }
    },
    "quick_replies": quickReplies
  }

  callFBSendAPI(senderId, response);
}