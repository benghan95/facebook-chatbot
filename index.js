/**
 * Module dependencies.
 */
const bodyParser = require('body-parser');
const chalk      = require('chalk');
const dotenv     = require('dotenv');
const express    = require('express');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env' });

/**
 * Controllers (route handlers).
 */
const webhookController = require('./controllers/webhook');
const profileController = require('./controllers/profile');

/**
 * Create Express server.
 */
const app = express();

/**
 * Express configuration.
 */
app.set('host', process.env.HOST || '0.0.0.0');
app.set('port', process.env.PORT || 8080);

app.use(bodyParser.json());

/**
 * Primary app routes.
 */
app.get('/', webhookController.verification);
app.post('/', webhookController.messageWebhook);

app.post('/messenger-profile', profileController.setProfile);
app.delete('/messenger-profile', profileController.resetProfile);


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
});